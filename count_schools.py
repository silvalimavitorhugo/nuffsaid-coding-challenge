import csv


def print_count():
    with open("school_data.csv", 'r', encoding="ISO-8859-1") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        # This skips the first row of the CSV file.
        next(csv_reader)
        TOTAL_NUMBER_OF_SCHOOLS = 0
        MLOCALE_SCHOOLS = {}
        STATES_SCHOOLS = {}
        CITY_SCHOOLS = {}
        for row in csv_reader:
            state = row[5]
            city = row[4]
            m_locale =row[8]
            if m_locale != "N":
                m_locale_obj = MLOCALE_SCHOOLS.get(m_locale)
                if m_locale_obj:
                    MLOCALE_SCHOOLS[m_locale] += 1
                else:
                    MLOCALE_SCHOOLS[m_locale] = 1

            state_obj = STATES_SCHOOLS.get(state)
            if state_obj:
                STATES_SCHOOLS[state] += 1
            else:
                STATES_SCHOOLS[state] = 1
            
            city_obj = CITY_SCHOOLS.get(city)
            if city_obj:
                CITY_SCHOOLS[city] += 1
            else:
                CITY_SCHOOLS[city] = 1

            TOTAL_NUMBER_OF_SCHOOLS += 1
        print(f"Total Schools: {TOTAL_NUMBER_OF_SCHOOLS}") 
        print("Schools by State:")
        for state, schools in STATES_SCHOOLS.items():
            print(f"{state}: {schools}")
        print("Schools by Metro-centric locale:")
        for key, number_of_schools in MLOCALE_SCHOOLS.items():
            print(f"{key}: {number_of_schools}")
        
        most_schools = max(CITY_SCHOOLS, key=CITY_SCHOOLS.get)
        print(f"City with most schools: {most_schools} ({CITY_SCHOOLS[most_schools]} schools)")
        print(f"Unique cities with at least one school: {len(CITY_SCHOOLS.keys())}")