import csv
import time


def preprocess_file():
    schools_data = {}
    with open("school_data.csv", 'r', encoding="ISO-8859-1") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        # This skips the first row of the CSV file.
        next(csv_reader)
        for row in csv_reader:
            state = row[5]
            city = row[4]
            school_name = row[3]
            school_id = row[0]
            schools_data[school_id] = {
                "state": state,
                "city": city,
                "school_name": school_name
            }
        return schools_data


def check_number_of_strings_matching(search_string_set, string_set):
    matching_words = string_set & search_string_set
    return len(matching_words)


def calculate_matching_school_info_value(school_name: str, state: str, city: str, string_query: set):
    school_name_set = set(school_name.split())
    matching_words_school_name = check_number_of_strings_matching(string_query, school_name_set)
    if (matching_words_school_name == len(string_query)) and (len(string_query) == len(school_name_set)):
        # The search string is the same size of the school name and all words matches the school_name
        return float("inf")

    city_state_string = city + " " + state
    city_state_set = set(city_state_string.split())

    full_string_search_set = school_name_set | city_state_set
    # Number of unique matching words on full string search
    full_string_matching_words = check_number_of_strings_matching(string_query, full_string_search_set)

    matching_words_city_state = check_number_of_strings_matching(string_query, city_state_set)
    # Increase matching value for results that have occurence of city or state 
    return full_string_matching_words  * (matching_words_city_state + 1)


def search_schools(string_query: str):
    schools_data = preprocess_file()
    
    start_time = time.time()
    school_matching_rank = {}
    string_query_set = set(string_query.lower().split())
    for school_id, value in schools_data.items():
        school_name = value["school_name"].lower()
        state = value["state"].lower()
        city = value["city"].lower()
        
        school_matching_value = calculate_matching_school_info_value(school_name, state, city, string_query_set)
        if school_matching_value:
            school_matching_rank[school_id] = school_matching_value
    
    if len(school_matching_rank.items()) > 3:
        sorted_matching_schools = sorted(school_matching_rank.items(), key=lambda x: x[1], reverse=True)[:3]
    else:
        sorted_matching_schools = sorted(school_matching_rank.items(), key=lambda x: x[1], reverse=True)

    for school_id, percentage in sorted_matching_schools:
        print(schools_data[school_id]["school_name"])
        print(schools_data[school_id]["city"], schools_data[school_id]["state"], percentage)
    print("--- %s seconds ---" % (time.time() - start_time))
